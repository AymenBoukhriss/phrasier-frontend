import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selectAll',
  // pure: false
})
@Injectable()
export class MyLds implements PipeTransform {
  transform(items: any[], param: string): any {
    // filter items array, items which match and return true will be kept, false will be filtered out
    if (param) {
      return items;
    }

    if (items) {
      return items.filter((item, index) => item.gender === param);
    }
  }
}
