import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AlertComponent } from '../_components/alert/alert.component';
import { MyModalComponent } from './my-modal.component';
import { ModalServiceModule } from '../_components/modal-service/modal-service.module';
import { ToggleButtonComponent } from './toggle-button.component';
import { TransmitLdsComponent } from '../main/transmit-lds/transmit-lds.component';
@NgModule({
  imports: [CommonModule, FormsModule, HttpModule, RouterModule],
  declarations: [AlertComponent,MyModalComponent],
  exports: [AlertComponent,MyModalComponent]
})
export class SharedModule {}