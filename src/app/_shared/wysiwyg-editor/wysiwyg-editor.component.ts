import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnInit,
    Output,
    ViewChild
  } from '@angular/core';
  import { isNullOrUndefined } from 'util';
import { TestComponent } from '../../main/sample/test/test.component';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
  
  
  @Component({
    selector: 'wysiwyg-editor',
    templateUrl: './wysiwyg-editor.component.html',
    styleUrls  : ['./wysiwyg-editor.component.scss'],
    
  })
  export class WysiwygEditorComponent implements OnInit, AfterViewInit {
    @Input() styleString: string;
    @Input() toolbarStyle: string;
    @ViewChild('editor') editor: ElementRef;
    @ViewChild('toolbar') toolbar: ElementRef;
  
    @Output() htmlChange = new EventEmitter(true);
    @Output() editableChange = new EventEmitter();
  
    private updateTimer: any;
    public hasFocus = false;
    private _html = '';
    public isEditable = true;
    public placeholder = true;
    @Output() buttonClick: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
    @Output() truncateButton: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
    @Output() saveButton: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
    @Output() transmitButton: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
    @Input()
    get editable() {
      return this.isEditable;
    }
  
    set editable(val) {
      this.isEditable = val;
      this.editor.nativeElement.contentEditable = this.isEditable;
      this.editableChange.emit(this.isEditable);
    }
  
    @Input()
    get html() {
      return this._html;
    }
  
    set html(val) {
      let broadcast = val;
      if (this.isEditable && this.hasFocus) {
        broadcast = broadcast.replace('<div', '<p').replace('</div>', '</p>');
        broadcast = broadcast.replace('<strong', '<b').replace('</strong>', '</b>');
        broadcast = broadcast.replace('<em', '<i').replace('</em>', '</i>');
        this.htmlChange.emit(broadcast);
      }
      this._html = broadcast;
  
      if (!this.hasFocus) {
        this.editor.nativeElement.innerHTML = this._html;
       
      }
    }
  
    map: Map<string, boolean> = new Map([
      ['bold', false],
      ['italic', false],
      ['underline', false],
      ['strikethrough', false],
      ['justifyleft', true],
      ['justifycenter', false],
      ['justifyright', false],
      ['justifyfull', false],
      ['p', false],
      ['h1', false],
      ['h2', false],
      ['h3', false],
      ['insertUnorderedList', false]
    ]);
  
    constructor(
      private matIconRegistry:MatIconRegistry,
      private domSanitizer:DomSanitizer
    ) {
      this.matIconRegistry.addSvgIcon(
        "transmit",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/share.svg")
      );
    }
  
    ngOnInit() {
        console.log(this.styleString);
    }
  
    ngAfterViewInit(): void {
      this.editor.nativeElement.contentEditable = this.isEditable;
      this.editor.nativeElement.innerHTML = this._html;
      this.fixPTag();
      this.updateHtml();
    }
  
    public onKeypress(event: KeyboardEvent): void {
      this.updateToolbar();
     
    }
  
    public onKeydown(event: KeyboardEvent): void {
      this.updateHtml();
      this.updateToolbar();
      this.fixBackspace(event);
      
    }
  
    private fixBackspace(event: KeyboardEvent) {
      console.log(event.keyCode);
      if (event.keyCode === 8 && (
        this.editor.nativeElement.innerHTML === '<p><br></p>'
        || this.editor.nativeElement.innerHTML === '<h1><br></h1>'
        || this.editor.nativeElement.innerHTML === '<h2><br></h2>'
        || this.editor.nativeElement.innerHTML === '<h3><br></h3>'
      )) {
        
        event.preventDefault();
      }
    }
  
    public onKeyup(event: KeyboardEvent): void {
      if (!event.ctrlKey && event.keyCode !== 17) {
        this.fixPTag();
      }
      this.updateHtml();
      this.updateToolbar();
      
    }
  
    public onFocus(event: FocusEvent): void {
      this.hasFocus = true;
      
      this.updateToolbar();
      console.log(this.html);
      console.log(this._html);
      console.log(this.editor.nativeElement.innerHTML);
      if(this._html.toString() === 'placeholder' || this._html.toString() ==='<p>placeholder</p>'){
        this.editor.nativeElement.innerHTML='<p><br></p>';
        this._html='<p><br></p>';
        this.html='<p><br></p>';
        }
      this.placeholder= this._html.toString() === '<p><br></p>'
    }
  
    public onBlur(event: FocusEvent): void {
      this.hasFocus = false;
      this.updateToolbar();
     
      if(this._html.toString() === '<p><br></p>' ||this._html.toString() === '' ){
        this.editor.nativeElement.innerHTML='<p>placeholder</p>';
        this._html='<p>placeholder</p>';
        this.html='<p>placeholder</p>';
      }
      this.placeholder= this._html.toString() === '<p><br></p>'
    }
  
    public updateHtml() {
      if (this.updateTimer !== null) {
        clearTimeout(this.updateTimer);
      }
      this.updateTimer = setTimeout(() => {
       
        this.html = this.editor.nativeElement.innerHTML;
        
      }, 300);
    }
  
    public onMousedown() {
      this.updateToolbar();
    }
  
    public onMouseup() {
      this.updateToolbar();
    }
  
    public updateToolbar() {
  
      this.map.forEach((value: boolean, command: string) => {
        this.map[command] = false;
        try {
          this.map[command] = document.queryCommandState(command);
        } catch (e) {
          // catch unknown commands (IE)
        }
      });
      const sel = window.getSelection();
      let range;
      try {
        range = sel.getRangeAt(0);
      } catch (e) {
        range = null;
      }
      if (range == null) {
        return;
      }
      try {
  
        let tag = range.commonAncestorContainer;
  
        while (!(['p', 'h1', 'h2', 'h3'].indexOf(tag.nodeName.toLowerCase()) > -1)) {
          tag = tag.parentNode;
        }
        this.map[tag.nodeName.toLowerCase()] = true;
      } catch (e) {
      }
    }
  
  
    private fixPTag(): void {
  
      if (!this.isEditable || !this.hasFocus) {
        return;
      }
      const elements = this.editor.nativeElement.childNodes;
      for (let i = 0; i < elements.length; i++) {
        const e = elements[i];
        const iEVersion = this.getInternetExplorerVersion();
        if (e.nodeType === 3 && iEVersion === -1 || e.nodeName.toLowerCase() === 'div') {
          const ptag = document.createElement('p');
          const br = document.createElement('br');
          ptag.innerText = e.textContent;
          if (e.nodeName.toLowerCase() === 'div') {
            ptag.appendChild(br);
          }
  
          let sel = document.getSelection();
  
          if (sel.baseNode && sel.baseNode.isEqualNode(e)) {
            let range = sel.getRangeAt(0);
            const offset = range ? range.startOffset : 0;
            const tmp = e.parentNode.replaceChild(ptag, e);
  
            range = document.createRange();
            sel = window.getSelection();
            const target = ptag.childNodes[0];
  
            if (!isNullOrUndefined(target)) {
              range.setStart(target, offset);
              range.collapse(true);
              sel.removeAllRanges();
              sel.addRange(range);
            }
  
  
          } else {
            const tmp = e.parentNode.replaceChild(ptag, e);
          }
        }
      }
  
      if (this.editor.nativeElement.innerText === '' || this.editor.nativeElement.innerHTML === '<br>') {
  
  
        const ptag = document.createElement('p');
        const br = document.createElement('br');
        ptag.appendChild(br);
        this.editor.nativeElement.innerHTML = '';
        this.editor.nativeElement.appendChild(ptag);
  
        const range = document.createRange();
        const sel = window.getSelection();
  
        const target = ptag.childNodes[0];
  
        range.setStartBefore(br);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
        console.log(sel);
      }
    }
  
    public execCommand(commandWithArgs, valueArg = null, event = null) {
      if (event !== null) {
        event.preventDefault();
        event.stopPropagation();
      }
      const commandArr = commandWithArgs.split(' '),
        command = commandArr.shift(),
        args = commandArr.join(' ') + (valueArg || '');
      if (command === 'createlink' || command === 'insertimage') {
        const url = prompt('Enter the link here: ', 'http:\/\/');
        if (url) {
          document.execCommand(command, false, url);
        }
      }else if(command ==='a'){
          
        } else {
        document.execCommand(command, false, args);
      }
      this.updateHtml();
      this.updateToolbar();
    }
  
    private getInternetExplorerVersion() {
      let rV = -1; // Return value assumes failure.
  
      if (navigator.appName === 'Microsoft Internet Explorer' || navigator.appName === 'Netscape') {
        const uA = navigator.userAgent;
        const rE = new RegExp('MSIE ([0-9]{1,}[\.0-9]{0,})');
  
        if (rE.exec(uA) != null) {
          rV = parseFloat(RegExp.$1);
        } else if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {
          rV = 11;
        }
      }
      return rV;
    }
     countLines() {
      var el = document.getElementById('content');
      var divHeight = el.offsetHeight
      var lineHeight = parseInt(el.style.lineHeight);
      var i=0;
      divHeight-=20
     
      while(divHeight%38!==0){
        console.log(divHeight%38);
        i++;
        divHeight-=20
      }
      var lines = divHeight / 38;
      console.log("Lines: " + (lines+i));
   }
   

   
  }