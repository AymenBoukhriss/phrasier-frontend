import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule , HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressBarModule, MatCardModule, MatMenuModule, MatProgressSpinnerModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { MainComponent } from './main/main.component';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { fakeBackendProvider } from './_helpers';

import { AuthGuard } from './_guards';
import { Role } from './_models';
import { Toast, ToastrModule } from 'ngx-toastr';
import { ListLdsComponent } from './main/list-lds/list-lds.component';
import { AlertService } from './_services';
import { AlertComponent } from './_components/alert/alert.component';
import { CreateUserComponent } from './main/create-user/create-user.component';
import { ApiService } from './_services/api.service';
import { TokenService } from './_services/token.service';
import { HttpModule } from '@angular/http';
import { LoginModule } from './main/login/login.module';
import { InitPasswordModule } from './main/init-password/init-password.module';
import { LoginComponent } from './main/login/login.component';
import { InitializePasswordComponent } from './main/init-password/init-password.component';
import { SharedModule } from './_shared/shared.module';
import { ForgetPasswordComponent } from './main/forget-password/forget-password.component';
import { ForgetPasswordModule } from './main/forget-password/forget-password.module';
import { ListTransmitedLdsComponent } from './main/trasmit-lds/trasmit-lds.component';
import { ListLdsNComponent } from './main/trasmit-lds/list-lds/list-lds.component';
import { TransmitLdsComponent } from './main/transmit-lds/transmit-lds.component';
import { BlocComponent } from './main/bloc/bloc.component';


const appRoutes: Routes = [
    {
    path: '', 
        component: MainComponent, children: [
            { path: '', loadChildren:() =>SampleModule, canActivate: [AuthGuard]},
            // { path: 'users', loadChildren: './pages/users/users.module#UsersModule', data: { breadcrumb: 'Users' } },
            // { path: 'dynamic-menu', loadChildren: './pages/dynamic-menu/dynamic-menu.module#DynamicMenuModule', data: { breadcrumb: 'Dynamic Menu' }  },          
            // { path: 'ui', loadChildren: './pages/ui/ui.module#UiModule', data: { breadcrumb: 'UI' } },
            // { path: 'mailbox', loadChildren: './pages/mailbox/mailbox.module#MailboxModule', data: { breadcrumb: 'Mailbox' } },
            // { path: 'chat', loadChildren: './pages/chat/chat.module#ChatModule', data: { breadcrumb: 'Chat' } },
            // { path: 'form-controls', loadChildren: './pages/form-controls/form-controls.module#FormControlsModule', data: { breadcrumb: 'Form Controls' } },
            // { path: 'tables', loadChildren: './pages/tables/tables.module#TablesModule', data: { breadcrumb: 'Tables' } },
            // { path: 'schedule', loadChildren: './pages/schedule/schedule.module#ScheduleModule', data: { breadcrumb: 'Schedule' } },
            // { path: 'maps', loadChildren: './pages/maps/maps.module#MapsModule', data: { breadcrumb: 'Maps' } },
            // { path: 'charts', loadChildren: './pages/charts/charts.module#ChartsModule', data: { breadcrumb: 'Charts' } },
            // { path: 'drag-drop', loadChildren: './pages/drag-drop/drag-drop.module#DragDropModule', data: { breadcrumb: 'Drag & Drop' } },
            // { path: 'icons', loadChildren: './pages/icons/icons.module#IconsModule', data: { breadcrumb: 'Material Icons' } },
            // { path: 'blank', component: BlankComponent, data: { breadcrumb: 'Blank page' } },
            // { path: 'search', component: SearchComponent, data: { breadcrumb: 'Search' } },
            // { path: 'search/:name', component: SearchComponent, data: { breadcrumb: 'Search' } }
        ]
    },
    //{ path: 'landing', loadChildren: './pages/landing/landing.module#LandingModule' },
    { path: 'login', loadChildren:() =>LoginModule },
    { path: 'init-password/:token/:email', loadChildren:() =>InitPasswordModule},
    { path: 'forgot-password', loadChildren:() =>ForgetPasswordModule},
    { path: '**', redirectTo:'' }
];

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        ListLdsNComponent,


        




        
    ],
    entryComponents : [
        Toast
    ],
    imports     : [
        
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),

        ToastrModule.forRoot(),
        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressBarModule,
        MatCardModule,
        MatMenuModule,
        MatProgressSpinnerModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        


        
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider,
        ApiService,
        TokenService,
    ],
    bootstrap   : [
        AppComponent,
    ],

    
})
export class AppModule
{
}
