import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitializePasswordComponent } from './init-password.component';

describe('InitializePasswordComponent', () => {
  let component: InitializePasswordComponent;
  let fixture: ComponentFixture<InitializePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitializePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitializePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
