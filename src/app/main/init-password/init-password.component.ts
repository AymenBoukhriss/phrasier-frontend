import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { first } from 'rxjs/operators';

import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, AlertService } from '../../_services';
import { RetypedPasswordErrorStateMatcher } from '../../_components/retyped-password-error.directive';



@Component({
    selector   : 'init-password',
    templateUrl: './init-password.component.html',
    styleUrls  : ['./init-password.component.scss'],
    animations : fuseAnimations
})
export class InitializePasswordComponent implements OnInit
{
    initPassword: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    matcher = new RetypedPasswordErrorStateMatcher();
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {FuseSplashScreenService} _fuseSplashScreenService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute,

    )
    {

        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        }

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        console.log(this.route.snapshot.paramMap.get("token"))
        this.initPassword = this._formBuilder.group({
            retypedPassword   : [''],
            password: ['', Validators.required]
        }, { validator: this.checkPasswords});
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    console.log(group.controls.password.value);
    let pass = group.controls.password.value;
    let confirmPass = group.controls.retypedPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }
    get f() { return this.initPassword.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.initPassword.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.resetPassword(this.f.password.value,this.route.snapshot.paramMap.get("token")).subscribe((res:any) => {
            if(res.error){
                this.alertService.error(res.message);
            }else{
                this.authenticationService.login(this.route.snapshot.paramMap.get("email"), this.f.password.value)
                .pipe(first())
                .subscribe(
                    data => {
      
                        this.router.navigate([this.returnUrl]);
                    },
                    error => {
                        this.error = error;
                        this.alertService.error("Username or password is incorrect");
                        this.loading = false;
                    });
            }
        })
      
    }
    
}
