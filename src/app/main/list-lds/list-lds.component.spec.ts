import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLdsComponent } from './list-lds.component';

describe('ListLdsComponent', () => {
  let component: ListLdsComponent;
  let fixture: ComponentFixture<ListLdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
