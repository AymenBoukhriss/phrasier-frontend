import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { LdsService } from '../../_services/lds.service';

@Injectable()
export class ListLdsService implements Resolve<any>
{
    ListLds: any[];
    onListChange: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _LdsService: LdsService
    )
    {
        // Set the defaults
        this.onListChange = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllLds()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getAllLds(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._LdsService.getAllLds()
                .subscribe((response: any) => {
                    this.ListLds = response;
                    this.onListChange.next(this.ListLds);
                    resolve(response);
                }, reject);
        });
    }
}
