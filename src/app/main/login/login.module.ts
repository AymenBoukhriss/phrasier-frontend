import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { LoginComponent } from './login.component';
import { AlertComponent } from '../../_components/alert/alert.component';
import { AlertService } from '../../_services';
import { SharedModule } from '../../_shared/shared.module';
import { FuseProgressBarModule } from '@fuse/components';



const routes = [
    {
        path: '', component: LoginComponent, pathMatch: 'full' 
    }
];

@NgModule({
    declarations: [
        LoginComponent,

    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        SharedModule,
        FuseSharedModule,
        FuseProgressBarModule
    ],
    providers:[
        AlertService

    ],

})
export class LoginModule
{
}
