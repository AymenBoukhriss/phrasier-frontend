import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserManagement } from '../../_services/user-management.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserService } from '../../_services';

@Component({
    selector   : 'forms',
    templateUrl: './create-user.component.html',
    styleUrls  : ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy
{
    form: FormGroup;
    formErrors: any;
    isLoading:boolean=false;
    userHierarchque:any;

    // Vertical Stepper
    verticalStepperStep1: FormGroup;
    verticalStepperStep2: FormGroup;
    verticalStepperStep3: FormGroup;
    verticalStepperStep4: FormGroup;
    verticalStepperStep1Errors: any;
    verticalStepperStep2Errors: any;
    verticalStepperStep3Errors: any;
    verticalStepperStep4Errors: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _formBuilder: FormBuilder,
        private _userManagement : UserManagement,
        private _ToastService : ToastrService,
        private router : Router,
        private _UserService : UserService
    )
    {
        // Reactive form errors
        this.formErrors = {
            company   : {},
            firstName : {},
            lastName  : {},
            email   : {},
            birthDate  : {},
            city      : {},
            state     : {},
            postalCode: {},
            country   : {}
        };

       

        // Vertical Stepper form error
        this.verticalStepperStep1Errors = {
            firstName: {},
            lastName : {}
        };

        this.verticalStepperStep2Errors = {
          email: {}
        };

        this.verticalStepperStep3Errors = {
          birthDate      : {},
          
        };
        this.verticalStepperStep4Errors = {
            role      : {},
            user : {}
          };
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    minDate:Date;
    maxDate:Date;
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        
        this.minDate = new Date(new Date().getFullYear()-80, 12);
        this.maxDate = new Date(new Date().getFullYear(), new Date().getMonth());
        // Reactive Form
        this.form = this._formBuilder.group({
            company   : [
                {
                    value   : 'Google',
                    disabled: true
                }, Validators.required
            ],
            firstName : ['', Validators.required],
            lastName  : ['', Validators.required],
            email   : ['', Validators.required],
            birthDate  : ['', Validators.required],
            city      : ['', Validators.required],
            state     : ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country   : ['', Validators.required]
        });

        this.form.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.onFormValuesChanged();
            });


        // Vertical Stepper form stepper
        this.verticalStepperStep1 = this._formBuilder.group({
            firstName: ['', Validators.required],
            lastName : ['', Validators.required]
        });

        this.verticalStepperStep2 = this._formBuilder.group({
          email: ['', Validators.required]
        });

        this.verticalStepperStep3 = this._formBuilder.group({
            birthDate      : ['', Validators.required],
           
        });
        this.verticalStepperStep4 = this._formBuilder.group({
            role      : ['', Validators.required],
            user      : ['',Validators.required]
           
        });
     
        this._UserService.getHierarchiqueUser().subscribe(res =>{
            this.userHierarchque = res;
        } )

        this.verticalStepperStep1.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.onFormValuesChanged();
            });

        this.verticalStepperStep2.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.onFormValuesChanged();
            });

        this.verticalStepperStep3.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.onFormValuesChanged();
            });
            this.verticalStepperStep4.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.onFormValuesChanged();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * On form values changed
     */
    onFormValuesChanged(): void
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }



    /**
     * Finish the vertical stepper
     */
    finishVerticalStepper(): void
    {
      this.isLoading=true;
        this._userManagement.createUser({
          lastName : this.verticalStepperStep1.controls["lastName"].value,
          firstName : this.verticalStepperStep1.controls["firstName"].value,
          username : this.verticalStepperStep2.controls["email"].value,
          birthDate : this.verticalStepperStep3.controls["birthDate"].value,
          role : this.verticalStepperStep4.controls["role"].value,
          parentId : this.verticalStepperStep4.controls["user"].value
        }).subscribe( res => {
          if(res.error){
            this._ToastService.error(res.message,"Error",{
              timeOut: 3000
            });
            this.isLoading=false;
          }else{
            this._ToastService.success(res.message,"Success",{
              timeOut: 3000
            });
            this.isLoading=false;
            this.router.navigateByUrl('/list-user');
          }
        }
        );
    }

}
