import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasmitLdsComponent } from './trasmit-lds.component';

describe('TrasmitLdsComponent', () => {
  let component: TrasmitLdsComponent;
  let fixture: ComponentFixture<TrasmitLdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasmitLdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasmitLdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
