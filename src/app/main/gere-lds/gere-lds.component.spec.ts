import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GereLdsComponent } from './gere-lds.component';

describe('GereLdsComponent', () => {
  let component: GereLdsComponent;
  let fixture: ComponentFixture<GereLdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GereLdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GereLdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
