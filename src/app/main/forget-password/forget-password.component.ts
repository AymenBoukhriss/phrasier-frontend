import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { first } from 'rxjs/operators';

import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, AlertService } from '../../_services';
import { RetypedPasswordErrorStateMatcher } from '../../_components/retyped-password-error.directive';
import { ToastrService } from 'ngx-toastr';



@Component({
    selector   : 'forget-password',
    templateUrl: './forget-password.component.html',
    styleUrls  : ['./forget-password.component.scss'],
    animations : fuseAnimations
})
export class ForgetPasswordComponent implements OnInit
{
  ForgetPasswordForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    matcher = new RetypedPasswordErrorStateMatcher();
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {FuseSplashScreenService} _fuseSplashScreenService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute,
        private toastr: ToastrService

    )
    {

        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        }

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        console.log(this.route.snapshot.paramMap.get("token"))
        this.ForgetPasswordForm = this._formBuilder.group({
            
            email: ['', [Validators.required, Validators.email]]
        });
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
   
    get f() { return this.ForgetPasswordForm.controls; }

    onSubmit() {
        this.submitted = true;
        this.alertService.clear();
        // stop here if form is invalid
        if (this.ForgetPasswordForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.forgotPassword(this.f.email.value).subscribe((res:any) => {
            if(res.error){
                this.alertService.error(res.message);
                this.loading = false;
            }else{
                this.toastr.success(res.message,"succée");
                this.router.navigateByUrl("/login");
            }
        })
      
    }
    
}
