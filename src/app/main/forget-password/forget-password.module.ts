import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';


import { AlertService } from '../../_services';
import { UserManagement } from '../../_services/user-management.service';
import { SampleModule } from '../sample/sample.module';
import { LoginModule } from '../login/login.module';
import { AlertComponent } from '../../_components/alert/alert.component';
import { SharedModule } from '../../_shared/shared.module';
import { ForgetPasswordComponent } from './forget-password.component';




const routes = [
    {
        path: '', component: ForgetPasswordComponent, pathMatch: 'full' 
    }
];

@NgModule({
    declarations: [
        ForgetPasswordComponent,
        
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        FuseSharedModule,
        SharedModule
    ],
    providers:[
        AlertService

    ],

})
export class ForgetPasswordModule
{
}
