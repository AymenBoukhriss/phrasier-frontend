import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService, AlertService } from '../../../../_services';
import { ToastrService } from 'ngx-toastr';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { CarouselDirective } from '../../../../_shared/carouse.directive';


@Component({
    selector     : 'contacts-contact-form-dialog',
    templateUrl  : './contact-form.component.html',
    styleUrls    : ['./contact-form.component.scss'],
    encapsulation: ViewEncapsulation.None,

    
})

export class VersionDialogComponent
{
    checked: boolean;
    blocs: any=[];
    user: any;
    contactForm: FormGroup;
    dialogTitle: string;
    value =false;
    value1;
    isLoading=false;
    isMailExist=false;
    width=100;
    slides:any;
    images = [
        '/some/image/a.png',
        '/other/image/b.png',
        '/third/image/c.png',
        '/more/images/d.png',
      ];
      selectedIndex=0;
    public config: SwiperConfigInterface = {
        direction: 'horizontal',
        slidesPerView: 1,
        keyboard: true,
        mousewheel: true,
        scrollbar: false,
        navigation: true,
        pagination: false
      };
    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<VersionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _UserService: UserService,
        private _AlertService : AlertService,
        private _fuseProgressBarService : FuseProgressBarService,
        private _ToastrService : ToastrService
    )
    {
        this.images = [
            '/some/image/a.png',
            '/other/image/b.png',
            '/third/image/c.png',
            '/more/images/d.png',
          ];
        // Set the defaults
        this.blocs = _data.res;
        this.width=(1/this.blocs.length)*100;
        this.slides = [
            {
              url: 'https://source.unsplash.com/1600x900/?nature,water'
            },
            {
              url: 'https://source.unsplash.com/1600x1600/?nature,forest'
            }
          ]
       console.log(this.blocs);

   
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    public onIndexChange(index: number) {
        console.log('Swiper index: ', index);
      }
    save(){
        this.matDialogRef.close(['save']);

        
    }
    receiveMessage($event) {
        console.log($event);
        this.selectedIndex = $event;
      }

}
