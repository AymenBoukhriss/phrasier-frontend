import { Component,ChangeDetectorRef } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { DndDropEvent } from "ngx-drag-drop";
import {ViewEncapsulation} from '@angular/core';
@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss'],
    
})

export class SampleComponent
{
  blocs=[];
  lds= {
    "date":"<p>Vincennes, le 21 novembre 2018</p>",
    "refN":"<p>N/Réf. : CODEP-PRS-2018-053322</p>",
    "destinataire":"<p>Monsieur le Professeur X, Directeur Monsieur le Docteur  Service de médecine nucléaire Hôpital Américain de Paris 63, boulevard Victor Hugo 92200 Neuilly-sur-Seine",
    "objet":"<p>Inspection sur le thème de la radioprotection </p>",
    "refD":"<p>[1] Code de l’environnement, notamment ses articles L. 592-19 et suivants.</p>",
    "body":"<p>Dans le cadre des attributions de l'Autorité de sûreté nucléaire (ASN) en références [1, 2 et 3] concernant le contrôle de la radioprotection, une inspection a eu lieu le 25 octobre 2018 dans votre établissement.</p>" ,
    "synthese":"<p>L’inspection du 25 octobre 2018 a été consacrée à l’examen, par sondage, des dispositions prises pour </p>",
    "types": [
      [
        {
          "title": "title1",
          "BlocN": "",
          "BlocT": "[]"
        },
        {
          "title": "title2",
          "BlocN": "[]",
          "BlocT": "[]"
        }
      ],
      [
        {
          "title": "title1",
          "BlocN": "[]",
          "BlocT": "[]"
        },
        {
          "title": "title2",
          "BlocN": "[]",
          "BlocT": "[]"
        }
      ],
      [
        {
          "title": "title1",
          "BlocN": "[]",
          "BlocT": "[]"
        },
        {
          "title": "title2",
          "BlocN": "[]",
          "BlocT": "[]"
        }
      ]
    ]
  };
  // lds = {
  //   "page" : [
  //           {
  //             "text":"<p>test</p>"
  //           },
  //           {
  //             "text":"<p>test2</p>"
  //           },
  //           {
  //             "text":"<p>test3</p>"
  //           },
  //           {
  //             "text":"<p>test4</p>"
  //           },
  //           {
  //             "text":"<p>test5</p>"
  //           },
  //           {
  //             "text":"<p>test6</p>"
  //           },
  //           {
  //             "text":"<p>test7</p>"
  //           }
  //   ]
  // };
    parentMessage={
    // borderBottom: '3px solid lightgreen'
    ['float']: 'right'
  };
  parentMessage2={
    // borderBottom: '3px solid lightgreen'
    ['float']: 'left'
  };
  parentMessage3={
    ['margin-left']:'350px ',
    ['float']: 'left'
    //['max-width']:'500px'
  };
  parentMessage4={
    ['margin-left']:'67px ',
    ['float']: 'left'
    //['max-width']:'500px'
  };
  parentMessage5={
    ['margin-left']:'27px ',
    ['float']: 'left',
    ['margin-top']:'5px'
    //['max-width']:'500px'
  };
  parentMessage6={
    // ['margin-left']:'27px ',
    ['float']: 'left',
    ['margin-top']:'5px',
    ['width']:'100%'
    //['max-width']:'500px'
  };
  parentMessage8={
    // ['margin-left']:'27px ',
    ['float']: 'left',
    ['margin-top']:'10px',
    ['width']:'100%'
    //['max-width']:'500px'
  };
  parentMessage7={
    // ['margin-left']:'27px ',
    ['float']: 'left',
    ['padding']: '20px',
    ['width']:'100%',
    ['border']: '1px solid #000'
  };
  toolbarStyle={
    ['text-align']: 'right'
  }
  text = 'Ang<b>ular 5foo</b>';
  text2 = 'Angular 5\nfoo';
  text3 = 'Angular 5\nfoo';
  text4 = 'Angular 5\nfoo';
  text5 = 'Angular 5\nfoo';
  text6 = 'Angular 5\nfoo';
  text7 = 'Angular 5\nfoo';
    public fruits:string[] = [
        "apple",
        "apple",
        "banana",
        "apple",
        "banana",
        "banana",
        "apple",
        "banana",
        "apple",
      ];
    
      public apples:string[] = [
        "apple",
        "apple"
      ];
    
      public bananas:string[] = [
        "banana",
        "banana"
      ];
      /*
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private cdr: ChangeDetectorRef,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private changeDetectorRef:ChangeDetectorRef
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
    }
    onDragged( item:any, list:any[] ) {

        const index = list.indexOf( item );
        //list.splice( index, 1 );
      }
    
      onDrop( event:DndDropEvent, i,j ) {
    
        let index = event.index;
    
        if( typeof index === "undefined" ) {
    
          index = 0;
        }
        console.log(i,j);
        this.lds.types[i][j].BlocN = event.data;
        this.changeDetectorRef.detectChanges();
        //list.splice( index, 0, event.data );
      }
      click(){
        console.log(this.lds.types[0][0].BlocN);
    }
    addBloc(){
      this.blocs.push({text:"<p>placeholder</p>"});
      console.log(this.blocs);
    }
}
