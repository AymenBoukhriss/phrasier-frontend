import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { SampleComponent } from './sample.component';

import { MatButtonModule, MatCheckboxModule,MatButtonToggleModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatProgressBarModule, MatRippleModule, MatSidenavModule, MatToolbarModule, MatTooltipModule, MatTableModule, MatVerticalStepper, MatStepperModule, MatProgressSpinnerModule, MatTabsModule, MatRadioModule } from '@angular/material';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { DndModule } from "ngx-drag-drop";
import { MatCardModule } from "@angular/material/card";
import {MatSelectModule} from "@angular/material/select"
import { ContentEditableFormDirective } from '../../_shared/content-editable-form.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WysiwygEditorComponent } from '../../_shared/wysiwyg-editor/wysiwyg-editor.component';
import { TestComponent } from './test/test.component';
import { FuseHighlightModule, FuseProgressBarModule, FuseConfirmDialogModule } from '@fuse/components';
import { ConfigService } from '../../_services/config.service';

import { HttpClientModule } from '@angular/common/http';

import { TokenService } from '../../_services/token.service';
import { BlocService, AlertService, UserService } from '../../_services';
import { MatPaginatorModule,MatSortModule } from '@angular/material';

import { NgxUiLoaderModule } from  'ngx-ui-loader';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ToastrModule, ToastrService, Toast } from 'ngx-toastr';
import { HighlightSearch } from '../../_shared/highlightsearch.pipe';
import { LdsService } from '../../_services/lds.service';
import { ListLdsComponent } from '../list-lds/list-lds.component';
import { CreateUserComponent } from '../create-user/create-user.component';
import { UserManagement } from '../../_services/user-management.service';
import { AlertComponent } from '../../_components/alert/alert.component';
import { ListUserComponent } from '../list-user/list-user.component';
import { EditUserDialog } from '../list-user/edit-user-dialog/edit-user.dialog';
import { ContactsContactFormDialogComponent } from '../list-user/contact-form/contact-form.component';
import { ToggleButtonComponent } from '../../_shared/toggle-button.component';
import { SwitchComponent } from '../../_shared/switch/switch.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { GereLdsComponent } from '../gere-lds/gere-lds.component';
import { VersionDialogComponent } from './test/contact-form/contact-form.component';
import { SwiperModule, SwiperConfigInterface,
    SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { CarouselComponent } from '../../_shared/carousel/carousel.component';
import { CarouselDirective } from '../../_shared/carouse.directive';
import { BannerCtrlDirective } from '../../_shared/banner.directive';
import { ListTransmitedLdsComponent } from '../trasmit-lds/trasmit-lds.component';
import { ListLdsNComponent } from '../trasmit-lds/list-lds/list-lds.component';
import { MyLds } from '../../_shared/myLds.pipe';
import { BlocComponent } from '../bloc/bloc.component';
import { BrowserModule } from '@angular/platform-browser';


const routes = [
    {
        path: '', component: SampleComponent, pathMatch: 'full' 
    },
    {
        path: 'lds-detail/:id', component: TestComponent
    },
    {
        path: 'list-lds', component: ListLdsComponent, pathMatch: 'full'
    },
    {
        path: 'create-user', component: CreateUserComponent, pathMatch: 'full'
    },
    {
        path: 'list-user', component: ListUserComponent, pathMatch: 'full'
    },
    {
        path: 'gere-lds', component: GereLdsComponent, pathMatch: 'full'
    },{
        path: 'list-transmitted-lds', component: ListTransmitedLdsComponent, pathMatch: 'full'

    },
    {
        path: 'blocs', component: BlocComponent, pathMatch: 'full'

    }
];
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    observer: true,
    direction: 'horizontal',
    threshold: 50,
    spaceBetween: 5,
    slidesPerView: 1,
    centeredSlides: true
  };
@NgModule({
    declarations: [

        SampleComponent,
        ContentEditableFormDirective,
        WysiwygEditorComponent,
        TestComponent,
        HighlightSearch,
        ListLdsComponent,
        CreateUserComponent,
        ListUserComponent,
        EditUserDialog,
        ContactsContactFormDialogComponent,
        ToggleButtonComponent,
        SwitchComponent,
        GereLdsComponent,
        VersionDialogComponent,
        CarouselComponent,
        CarouselDirective,

        BannerCtrlDirective,
        ListTransmitedLdsComponent,
        MyLds,
        BlocComponent

    ]

    ,
    imports     : [

        RouterModule.forChild(routes),
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatTableModule,
        SwiperModule,
        MatTabsModule,
        MatRadioModule,
        ToastrModule.forRoot(),
        MatSnackBarModule,
        NgxUiLoaderModule,
        TranslateModule,
        MatSelectModule ,
        MatSortModule,
        FuseSharedModule,
        MatButtonModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTooltipModule,
        MatInputModule,
        MatCardModule,
        NgxDnDModule,
        MatButtonToggleModule,
        DndModule,
        FuseHighlightModule,
        MatStepperModule,
        FuseProgressBarModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        FuseConfirmDialogModule,
        
        
    ],
    exports     : [
        SampleComponent,
        

    ],
    providers:[
        ConfigService,

        BlocService,
        ToastrService,
        LdsService,
        UserManagement,
        UserService,
        {
            provide: SWIPER_CONFIG,
            useValue: DEFAULT_SWIPER_CONFIG
        }
        
    ],
    entryComponents:[
        Toast,
        EditUserDialog,
        ContactsContactFormDialogComponent,
        VersionDialogComponent,
        CarouselComponent,
        ListTransmitedLdsComponent,


    ]
    
    
})

export class SampleModule
{
}
