import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransmitLdsComponent } from './transmit-lds.component';

describe('TransmitLdsComponent', () => {
  let component: TransmitLdsComponent;
  let fixture: ComponentFixture<TransmitLdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransmitLdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransmitLdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
