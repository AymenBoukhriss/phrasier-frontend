import { Component, AfterViewInit,ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { merge, Observable, BehaviorSubject, fromEvent, Subject, throwError,of  } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as htmlToPdfMake  from "../../../assets/index.js"
import {DataSource} from '@angular/cdk/collections';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { BlocService } from '../../_services/index.js';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-bloc',
  templateUrl: './bloc.component.html',
  styleUrls: ['./bloc.component.scss'],
   animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BlocComponent  implements AfterViewInit {
  isLoading:boolean=true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource;
  bloc:any;
  displayedColumns = ['ID', 'domaine','partie', 'Etat', 'Action'];
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  constructor(private _blocService:BlocService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router : Router,
    private toastr: ToastrService,
    private _fuseProgressBarService: FuseProgressBarService) { 
      this.matIconRegistry.addSvgIcon(
        "preview",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/preview.svg")
      );
      this.matIconRegistry.addSvgIcon(
        "edit",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/edit.svg")
      );
      this.matIconRegistry.addSvgIcon(
        "transmit",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/share.svg")
      );
      this.matIconRegistry.addSvgIcon(
        "rejected",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/rejected.svg")
      );

      this._blocService.getAll().subscribe(res=> {this.dataSource = new ExampleDataSource(res); this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this._fuseProgressBarService.hide();
        this.isLoading=false;

      })


        
    }
    ngAfterViewInit() {
      this._fuseProgressBarService.show();
      setTimeout(() => {this.dataSource.paginator = this.paginator;});
     
      this.dataSource.sort = this.sort;
    
  }
  getEtat(lds){
    switch(lds.etat){
      case "notShared" : return "Refusé";
      case "request" : return "En attente";
      case "shared": return "Accepté"
    }
  }

  valider(bloc){
    this._fuseProgressBarService.show();
    this._blocService.acceptRequest(bloc.id).subscribe(res =>{
      if(res.error){
        this.toastr.error(res.error,"Erreur");
        this._fuseProgressBarService.hide();
      }else{
        this.toastr.success("Demande de partage Accepté","Succès");
        this._fuseProgressBarService.hide();
        bloc.etat="shared";
      }
    })
  }

  reject(bloc){
    this._fuseProgressBarService.show();
    this._blocService.rejectRequest(bloc.id).subscribe(res =>{
      if(res.error){
        this.toastr.error(res.error,"Erreur");
        this._fuseProgressBarService.hide();
      }else{
        this.toastr.success("Demande de partage refusé","Succès");
        this._fuseProgressBarService.hide();
        bloc.etat="notShared";
      }
    })
  }

}
export class ExampleDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  private data;
  constructor(private dataa){
    super();
    this.data=dataa;
  }
  connect(): Observable<Element[]> {
    const rows = [];
    this.data.forEach(element => rows.push(element, { detailRow: true, element }));
    console.log(rows);
    return of(rows);
  }

  disconnect() { }
}