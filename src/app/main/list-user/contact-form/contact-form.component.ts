import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService, AlertService } from '../../../_services';
import { ToastrService } from 'ngx-toastr';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';


@Component({
    selector     : 'contacts-contact-form-dialog',
    templateUrl  : './contact-form.component.html',
    styleUrls    : ['./contact-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactsContactFormDialogComponent
{
    checked: boolean;
    action: string;
    user: any;
    contactForm: FormGroup;
    dialogTitle: string;
    value =false;
    value1;
    isLoading=false;
    isMailExist=false;

    changed() {
      this.value1 = this.value;
    }
    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ContactsContactFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _UserService: UserService,
        private _AlertService : AlertService,
        private _fuseProgressBarService : FuseProgressBarService,
        private _ToastrService : ToastrService
    )
    {
        // Set the defaults
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Contact';
            this.user = _data.user;
        }
        else
        {
            this.dialogTitle = 'New Contact';
            this.user = null;
        }

        this.contactForm = this.createContactForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup
    {
        console.log(this.user.firstName);
        this.value = this.user.is_active;
        return this._formBuilder.group({
            id      : [this.user.id],
            firstName    : [this.user.first_name],
            lastName: [this.user.last_name],
            email   : [this.user.username,Validators.email/*, this.isEmailUnique.bind(this)*/],
            birthDate: [this.user.birth_date],
            isActive:[this.user.is_active]
            });
    }
    save(){
        this._fuseProgressBarService.show();
        this.isMailExist=false;
        this.isLoading = true;
        console.log("teeet "+this.contactForm.getRawValue().firstName)
        this._UserService.modifyUser({
            id : this.contactForm.getRawValue().id,
            firstName : this.contactForm.getRawValue().firstName,
            lastName : this.contactForm.getRawValue().lastName,
            email : this.contactForm.getRawValue().email,
            birthDate : this.contactForm.getRawValue().birthDate,
            isActive : this.contactForm.getRawValue().isActive,
        }).subscribe( (res:any) => {
            if(!res.error){
                this._AlertService.success("Operation est effectué avec succée");
                this.isLoading=false;
                this._fuseProgressBarService.hide();
                this.matDialogRef.close(['save',this.contactForm]);
                
            }else{
                console.log(res.message);
                //this._AlertService.error(res.message+"");
                this._fuseProgressBarService.hide();
                this._ToastrService.error(res.message,"Error");
                this.isMailExist=true;
                this.isLoading=false;
            }
        },
            (error) => {
                this._fuseProgressBarService.hide();
                this._AlertService.error(error.exception[0]);
                this.isLoading=false;
            }
        

        )
        
    }
    // isEmailUnique(control: FormControl) {
    //     const q = new Promise((resolve, reject) => {
    //       setTimeout(() => {
    //         this._UserService.isEmailRegisterd(control.value).subscribe(() => {
    //           resolve(null);
    //         }, () => { resolve({ 'isEmailUnique': true }); });
    //       }, 1000);
    //     });
    //     return q;
    //   }
}

export class User {
    firstName:string;
    lastName:string;
    birthDate:string;
    email:string;
    id:number;
}