import {Component, Inject, OnInit} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Component({
    selector: 'edit-user-dialog',
    templateUrl: 'edit-user.dialog.html',
    styleUrls: ['edit-user.dialog.scss']
  })
  export class EditUserDialog implements OnInit{
    form: FormGroup;
    formErrors: any;

        // Private
    private _unsubscribeAll: Subject<any>;
    constructor(
        private _formBuilder: FormBuilder,
      public dialogRef: MatDialogRef<EditUserDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any) { 
        this.formErrors = {
            company   : {},
            firstName : {},
            lastName  : {},
            address   : {},
            address2  : {},
            city      : {},
            state     : {},
            postalCode: {},
            country   : {}
        };
        this._unsubscribeAll = new Subject();

      }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  ngOnInit(){
       // Reactive Form
       this.form = this._formBuilder.group({
         id      : ["this.contact.id"],
            name    : ["this.contact.name"],
            lastName: ["this.contact.lastName"],
            avatar  : ["this.contact.avatar"],
            nickname: ["this.contact.nickname"],
            company : ["this.contact.company"],
            jobTitle: ["this.contact.jobTitle"],
            email   : ["this.contact.email"],
            phone   : ["this.contact.phone"],
            address : ["this.contact.address"],
            birthday: ["this.contact.birthday"],
            notes   : ["this.contact.notes"]
    });
      


 
  }
  get f() { return this.form.controls; }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }
  }