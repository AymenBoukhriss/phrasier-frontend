import { Component, AfterViewInit,ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { LdsService } from '../../_services/lds.service';
import { merge, Observable, BehaviorSubject, fromEvent, Subject, throwError } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as htmlToPdfMake  from "../../../assets/index.js"
import {DataSource} from '@angular/cdk/collections';
import { Lds } from '../../_models/lds';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { UserService } from '../../_services';
import { MyModalComponent } from '../../_shared/my-modal.component';
import { EditUserDialog } from './edit-user-dialog/edit-user.dialog';
import { ContactsContactFormDialogComponent } from './contact-form/contact-form.component';
import { FormGroup } from '@angular/forms';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';



@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
  animations : fuseAnimations
})
export class ListUserComponent implements AfterViewInit {
  private lds:Lds;
  dataSource: MatTableDataSource<any>;
  isLoading = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  displayedColumns = ['ID', 'Email','Nom', 'Prénom','Role', 'Date/N','Date de creation', 'Activé','Action'];
  constructor(private _UserService:UserService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router : Router,
    private toastr: ToastrService,
    public dialog:MatDialog,
    private _fuseProgressBarService: FuseProgressBarService) { 
      this.matIconRegistry.addSvgIcon(
        "preview",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/preview.svg")
      );
      this.matIconRegistry.addSvgIcon(
        "edit",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/edit.svg")
      );
      this.matIconRegistry.addSvgIcon(
        "transmit",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/images/share.svg")
      );

      this._UserService.getAll().subscribe(res=> {this.dataSource = new MatTableDataSource(res); this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this._fuseProgressBarService.hide();
        this.isLoading=false;
      })
    }

    ngAfterViewInit() {
      this._fuseProgressBarService.show();
      setTimeout(() => this.dataSource.paginator = this.paginator);
     
      this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
 
  getRole(role){
    switch(role){
      case 'ROLE_ADMIN' : return "Administrateur";
      case 'ROLE_COMITE' : return "Comité";
      case 'ROLE_INSPECTEUR_HIERARCHIQUE' : return "Inspecteur Hierarchique";
      case 'ROLE_INSPECTEUR' : return "Inspecteur";
    }
  }

  remove(id){
    this._UserService.removeUserById(id).subscribe(res => {
      this._UserService.getAll().subscribe(res=> this.dataSource = new MatTableDataSource(res))
      if(!res.error.message){
        this.toastr.success("Supression effectué avec succée", 'Confirmation', {
          timeOut: 3000
        })
      }else{
        this.toastr.error("Operation echouée", 'Erreur', {
          timeOut: 3000
        })
      }
    })


  }

  createUser(){
    this.router.navigateByUrl("/create-user")
  }
  openDialog(user): void {
    // let dialogRef = this.dialog.open(EditUserDialog, {

    //   panelClass: 'contact-form-dialog',
 
    //   data: { name: "test", animal: "test" }
    // });
    console.log(user);
    let dialogRef = this.dialog.open(ContactsContactFormDialogComponent, {
      panelClass: 'contact-form-dialog',
      data      : {
          user: user,
          action : 'edit'
      }
  });

  dialogRef.afterClosed()
  .subscribe(response => {
      if ( !response )
      {
          return;
      }
      const actionType: string = response[0];
      const formData: FormGroup = response[1];
      switch ( actionType )
      {
          /**
           * Save
           */
          case 'save':
            console.log(formData.getRawValue());
            console.log(formData.getRawValue());
             user.username=formData.getRawValue().email;
             user.birth_date=formData.getRawValue().birthDate;
             user.first_name=formData.getRawValue().firstName;
             user.last_name=formData.getRawValue().lastName;
             user.is_active = formData.getRawValue().isActive;
              break;
          /**
           * Delete
           */
          case 'delete':
          console.log(formData.getRawValue());
              //this.deleteContact(contact);
              this.deleteUser(user);
              break;
      }
  });
}
deleteUser(user): void
{

  this._fuseProgressBarService.show();
    let confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
        disableClose: false
    });

    confirmDialogRef.componentInstance.confirmMessage = "Êtes-vous sûr de vouloir supprimer l'utilisateur ?";

    confirmDialogRef.afterClosed().subscribe(result => {
        if ( result )
        {
          this._UserService.removeUserById(user.id).subscribe(res => {
            this._UserService.getAll().subscribe(res=> this.dataSource = new MatTableDataSource(res))
            if(!res.error.message){
              this.toastr.success("Supression effectué avec succée", 'Confirmation', {
                timeOut: 3000
              })

              this._fuseProgressBarService.hide();
            }else{
              this.toastr.error("Operation echouée", 'Erreur', {
                timeOut: 3000
              })

              this._fuseProgressBarService.hide();
            }
          })
        }
       confirmDialogRef = null;
    });
  
}
}
export class LdsDataSource extends DataSource<any> {
  constructor(private _UserService:UserService) {
    super();
  }
  connect(): Observable<any> {
 
    return this._UserService.getAll();
  }
  disconnect() {}
}
