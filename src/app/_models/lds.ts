import { Type } from "./type";


export class Lds {
    "id": number;
    "date": string;
    "refN": string;
    "destinataire": string;
    "objet": string;
    "refD": string;
    "body": string;
    "synthese": string;
    "partie" =[[],[],[],[]];
}