import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
       // translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'Users',
                title    : 'User',
               // translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'account_box',
                url      : '/list-user',

            },
            {
                id       : 'list-lds',
                title    : 'Liste de Lds',
//              translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'ballot',
                url      : '/list-lds',
            },
            {
                id       : 'transmited-lds',
                title    : 'Liste de Lds transmise',
//              translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'ballot',
                url      : '/list-transmitted-lds',
            },
            {
                id       : 'blocs',
                title    : 'Liste de demande de partage',
//              translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'ballot',
                url      : '/blocs',
            },

        ]
    }
];
