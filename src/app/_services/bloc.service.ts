import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class BlocService {

  constructor(private _apiService: ApiService) { }

  getBlocNationalBySearchParameter(data): Observable<any> {
    return this._apiService.getWithParam('bloc-nationall',data).map(response => response);
  }
  shareBloc(data){
    return this._apiService.post('share',data);
  }
  getVersion(description){
    return this._apiService.get('version?description='+description);
  }
  getAll(){
    return this._apiService.get("sharedBloc");
  }
  rejectRequest(id){
    return this._apiService.get("bloc-reject?id="+id);
  }
  acceptRequest(id){
    return this._apiService.get("bloc-accept?id="+id);
  }
}