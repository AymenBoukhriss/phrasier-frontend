import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {
    private baseURL = "http://127.0.0.1:8000/api/";
  constructor(private _http: Http, private _tokenService: TokenService, private _router:Router) {
  }

  get(url: string): Observable<any> {
    // if (!this._tokenService.isTokenActive()) {
    //   this._tokenService.refreshToken();
    // }
    const headers: Headers = new Headers({
      'Authorization': 'Bearer ' + this._tokenService.getToken()
    });
    return this._http.get(
      `${this.baseURL}${url}`,
      {
        headers: headers
      })
      .map(response => response.json())
      .catch(this._handleError);
  }

  post(url: string, params: object): Observable<object> {
    // if (!this._tokenService.isTokenActive()) {
    //   this._tokenService.refreshToken();
    // }
    const headers: Headers = new Headers({
      'Authorization': 'Bearer ' + this._tokenService.getToken(),
      'Content-Type': 'application/json'
    });
    
    return this._http.post(
      `${this.baseURL}${url}`,
      params,
      {
        headers: headers
      })
      .map(response => response.json())
      .catch(this._handleError);
  }

  postFile(url: string, fileData: FormData): Observable<object> {
    // if (!this._tokenService.isTokenActive()) {
    //   this._tokenService.refreshToken();
    // }
    const headers: Headers = new Headers({
      'Authorization': 'Bearer ' + this._tokenService.getToken()
    });
    return this._http.post(
      `${this.baseURL}${url}`,
      fileData,
      {
        headers: headers
      })
      .map(response => this.extractData(response))
      //.catch(this._handleError);
  }

  private _handleError(error: Response): Observable<object> {
    console.log(error.json());
    // if(error.json().code == 401){
    //   this._router.navigateByUrl('/login')
    // }
    return  throwError(error.json());
  }

  getWithParam(url: string,data): Observable<object> {
    // if (!this._tokenService.isTokenActive()) {
    //   this._tokenService.refreshToken();
    // }
    const headers: Headers = new Headers({
      'Authorization': 'Bearer ' + this._tokenService.getToken(),
      'Content-Type': 'application/json'
    });
  
    return this._http.get(
      `${this.baseURL}${url}`,
      {
        //headers: headers
        headers:headers,
        params:data
      }
   )
      .map(response => response.json())
      .catch(this._handleError);
  }
  private extractData(res: Response) {
    let body = res.json();
    console.log("Body Data = "+body.data);
    return body.data || [];
  }

  delete(url: string): Observable<object> {
    // if (!this._tokenService.isTokenActive()) {
    //   this._tokenService.refreshToken();
    // }
    const headers: Headers = new Headers({
      'Authorization': 'Bearer ' + this._tokenService.getToken(),
      'Content-Type': 'application/json'
    });
    
    return this._http.delete(
      `${this.baseURL}${url}`,

      {
        headers: headers
      })
      .map(response => response.json())
      .catch(this._handleError);
  }

  put(url: string, params: object): Observable<object> {
    // if (!this._tokenService.isTokenActive()) {
    //   this._tokenService.refreshToken();
    // }
    const headers: Headers = new Headers({
      'Authorization': 'Bearer ' + this._tokenService.getToken(),
      'Content-Type': 'application/json'
    });
    
    return this._http.put(
      `${this.baseURL}${url}`,
      params,
      {
        headers: headers
      })
      .map(response => response.json())
      .catch(this._handleError);
  }
}