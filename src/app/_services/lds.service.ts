import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs/Rx';
import { Lds } from '../_models/lds';

@Injectable()
export class LdsService {

  constructor(private _apiService: ApiService) { }

  saveLdS(data): Observable<any> {
    return this._apiService.post('autoSave',data).map(response => response);
  }
  getLdsById(id):Observable<Lds> {
    return this._apiService.get('get-lds-by-id?id='+id).map(response => response["data"]);
  }
  getAllLds():Observable<any> {
    return this._apiService.get('get-lds').map(response => response["data"]);
  }
  removeLdsById(id):Observable<any>{
    return this._apiService.delete("remove-lds?id="+id);
  }
  createNewLds():Observable<any>{
    return this._apiService.get("new-lds");
  }
  transmitLds(id):Observable<any>{
    return this._apiService.get("transmit-lds?id="+id);
  }
  getTransmitLds():Observable<any>{
    return this._apiService.get("transmit");
  }
  rejectLds(id):Observable<any>{
    return this._apiService.get("reject?id="+id);
  }
  validerLds(id):Observable<any>{
    return this._apiService.get("valider?id="+id);
  }
}