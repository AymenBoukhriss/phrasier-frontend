import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class UserManagement {

  constructor(private _apiService: ApiService) { }

  createUser(data): Observable<any> {
    return this._apiService.post('register',data).map(response => response);
  }
  resetPassword(password,token){
    return this._apiService.post("reset_password",{token:token,password:password});
}
}