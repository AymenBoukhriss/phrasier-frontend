import { Injectable } from '@angular/core';

import {Observable} from 'rxjs/Rx';
import { User } from '../_models';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private _apiService: ApiService) { }

    getAll() : Observable<any>  {
        return this._apiService.get(`users`);
    }

    getById(id: number) {
        return this._apiService.get(`users/${id}`);
    }
    modifyUser(user){
        return this._apiService.put(`users/${user.id}`,user);
    }

    removeUserById(id:number):Observable<any>{
        return this._apiService.delete(`users/${id}`);
    }

    isEmailRegisterd(email: string) {
        return this._apiService.post('isEmailRegisterd', { email: email })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }
    
    getHierarchiqueUser(){
        return this._apiService.get('user-hierarchique');
    }

    private handleError(error: any) {
        console.log(error);
        return Observable.throw(error.json());
        ;
    }
}