import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class ConfigService {

  constructor(private _apiService: ApiService) { }

  getDomaineThematique(): Observable<any> {
    return this._apiService.get('domaine-thematique').map(response => response['data']);
  }
}