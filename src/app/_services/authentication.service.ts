import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../_models';
import { ApiService } from './api.service';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient,private _apiService:ApiService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post(`http://localhost:8000/login_check`, { username, password })
            .pipe(map((user:any) => {
                // login successful if there's a jwt token in the response
           
                
                if (user && user.token && user.data.isActive) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    localStorage.setItem('session_token', user.token);
                    localStorage.setItem('session_expiry', 3600+"");
                    this.currentUserSubject.next(user);
                }
                

                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    resetPassword(password,token){
        return this._apiService.post("reset_password",{token:token,password:password});
    }

    forgotPassword(email){
        return this._apiService.post("forget_password",{email:email});
    }
}