import {Injectable} from '@angular/core';
import {Http, Headers, URLSearchParams, Response} from '@angular/http';
//import {Config} from '../../../config/config.service';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TokenService {
    private baseURL = "http://127.0.0.1:8000/";
  constructor(private _http: Http) {
  }
  saveToken(token){
    localStorage.setItem('session_token', token);
      localStorage.setItem('session_expiry', 3600+"");
  }
  getNewToken(): Observable<object> {
    const headers: Headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const data: URLSearchParams = new URLSearchParams();
    //data.append('client_id', Config.CLIENT_ID);
    //data.append('client_secret', Config.CLIENT_SECRET);
    //data.append('grant_type', 'client_credentials');
    return this._http.post(
      `${this.baseURL}oauth/access_token`,
      data,
      {
        headers: headers
      })
      .map(response => response.json())
      .catch(this._handleError);
  }

  getToken(): string {
    return localStorage.getItem('session_token');
  }

  isTokenActive(): boolean {
    const currentDate: number = Math.round(Date.now() / 1000); // Convert milliseconds since UNIX epoch to seconds
    return (
      localStorage.getItem('session_token') !== '' &&
      localStorage.getItem('session_expiry') !== '' &&
      parseInt(localStorage.getItem('session_expiry'), 10) >= currentDate
     );
  }

  refreshToken(): void {
    this.getNewToken().subscribe(response => {
      localStorage.setItem('session_token', response['token']);
      localStorage.setItem('session_expiry', 3600+"");
    });
  }

  private _handleError(error: Response): Observable<object> {
    console.log(error.json());
    return Observable.throw(error.json());
  }
}